import {splitOnDoubleLetter} from ".";

it("should return split words", () => {
  expect(splitOnDoubleLetter("Letter")).toEqual(["let", "ter"]);
  expect(splitOnDoubleLetter("Really")).toEqual(["real", "ly"]);
  expect(splitOnDoubleLetter("Happy")).toEqual(["hap", "py"]);
  expect(splitOnDoubleLetter("Shall")).toEqual(["shal", "l"]);
  expect(splitOnDoubleLetter("Tool")).toEqual(["to", "ol"]);
  expect(splitOnDoubleLetter("Mississippi")).toEqual(["mis", "sis", "sip", "pi"]);
  expect(splitOnDoubleLetter("Easy")).toEqual([]);
});

it("should return an empty array", () => {
  expect(splitOnDoubleLetter("")).toEqual([]);
});
