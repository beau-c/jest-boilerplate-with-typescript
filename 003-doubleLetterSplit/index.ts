export function splitOnDoubleLetter(word) {
  word = word.toLowerCase();
  const result = [];

  for(let i=1; i<word.length; i++) {
    if (word.charAt(i) === word.charAt(i-1)) {
      result.push(word.substr(0,i));
      let remainder = word.substr(i);
      if (splitOnDoubleLetter(remainder).length > 0) {
        word = remainder;
        i = 1;
      }
      else {
        result.push(remainder);
      }
    }
  }

  return result;
}
