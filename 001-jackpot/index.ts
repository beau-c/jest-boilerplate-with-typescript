export function testJackpot(machineOutcome) {

  let firstSymbol = machineOutcome[0];

  for(let i=1; i<machineOutcome.length; i++) {
    if (machineOutcome[i] !== firstSymbol) {
      return false;
    }
  }

  return true;
}
