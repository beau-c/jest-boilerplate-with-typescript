import { testJackpot } from ".";

it("should return true with 4 identical symbols", () => {
  expect(testJackpot(["@", "@", "@", "@"])).toBe(true);
});

it("should return false with any different symbol", () => {
  expect(testJackpot(["s", "S", "S", "S"])).toBe(false);
  expect(testJackpot(["N", "n", "N", "N"])).toBe(false);
  expect(testJackpot(["$", "$", "S", "$"])).toBe(false);
  expect(testJackpot([1, "1", "1", "1"])).toBe(false);
});

it("should return true with 4 identical strings", () => {
  expect(testJackpot(["☺", "☺", "☺", "☺"])).toBe(true);
});

it("should return true with 4 identical numbers", () => {
  expect(testJackpot([4, 4, 4, 4])).toBe(true);
});

it("should return true with 4 identical bools", () => {
  expect(testJackpot([false, false, false, false])).toBe(true);
});
