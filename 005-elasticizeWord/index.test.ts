import {elasticize} from ".";

it("should pass the test case", () => {
  expect(elasticize("KAYAK")).toBe("KAAYYYAAK");
});

it("should pass the test case", () => {
  expect(elasticize("X")).toBe("X");
});

it("should pass the test case", () => {
  expect(elasticize("ANNA")).toBe("ANNNNA");
});

it("should pass the test case", () => {
  expect(elasticize("LOL")).toBe("LOOL");
});