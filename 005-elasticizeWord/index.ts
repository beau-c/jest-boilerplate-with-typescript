export function elasticize(word) {

  let result = "";
  let reverse= "";

  let halfLen = word.length/2;
  
  for(let i=0; i<halfLen; ++i) {
    for(let j=0; j<=i; ++j) {
      result += word.charAt(i);

      // Append to the reversed half if this is not the center character
      if (i != Math.floor(halfLen)) {
        reverse = word.charAt(word.length-1-i) + reverse;
      }
    }
  }

  return result + reverse;
}