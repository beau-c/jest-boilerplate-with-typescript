export function checkPasswordStrength(password) {
  if (password.length < 6 || password.includes(" ")) {
    return "Invalid";
  }

  let criteriaPass = [false, false, false, false, false];

  if (password.match(/(?=.*[a-z])/)) {
    criteriaPass[0] = true;
  }
  if (password.match(/(?=.*[A-Z])/)) {
    criteriaPass[1] = true;
  }
  if (password.match(/(?=.*\W)/)) {
    criteriaPass[2] = true;
  }
  if (password.length >= 8) {
    criteriaPass[3] = true;
  }
  if (password.match(/(?=.*[0-9])/)) {
    criteriaPass[4] = true;
  }

  const strength = criteriaPass.reduce((p,c) => p + c, 0);

  if (strength < 3) {
    return "Weak";
  }
  if (strength < 5) {
    return "Moderate";
  }

  return "Strong";
}
