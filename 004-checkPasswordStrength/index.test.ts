import {checkPasswordStrength} from ".";

it("should check valid passwords", () => {
  expect(checkPasswordStrength("stonk")).toBe("Invalid");
  expect(checkPasswordStrength("pass word")).toBe("Invalid");
});

it("should determine weak passwords", () => {
  expect(checkPasswordStrength("password")).toBe("Weak");
  expect(checkPasswordStrength("11081992")).toBe("Weak");
});

it("should determine moderate passwords", () => {
  expect(checkPasswordStrength("mySecurePass123")).toBe("Moderate");
  expect(checkPasswordStrength("!@!pass1")).toBe("Moderate");
});

it("should find strong passwords", () => {
  expect(checkPasswordStrength("@S3cur1ty")).toBe("Strong");
  expect(checkPasswordStrength("V3ryStr0ngP4$$w0rd!")).toBe("Strong");
});
