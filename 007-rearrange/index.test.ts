import { rearrangeSentence } from ".";

it("should pass test case 1", () => {
  expect(rearrangeSentence("is2 Thi1s T4est 3a")).toBe("This is a Test");
});

it("should pass test case 2", () => {
  expect(rearrangeSentence("4of Fo1r pe6ople g3ood th5e the2")).toBe("For the good of the people");
});

it("should pass test case 3", () => {
  expect(rearrangeSentence("5weird i2s JavaScri1pt dam4n so3")).toBe("JavaScript is so damn weird");
});

it("should ignore a string without numbers", () => {
  expect(rearrangeSentence("no numbers in string")).toBe("no numbers in string");
});

it("should return a single word", () => {
  expect(rearrangeSentence("He1llo")).toBe("Hello");
});

it("should return an empty string", () => {
  expect(rearrangeSentence(" ")).toBe("");
});
