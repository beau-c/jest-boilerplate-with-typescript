export function rearrangeSentence(input) {
  let words = input.match(/\w+/g);

  if (!words)
    return ""; // No words found

  words.sort((w1, w2) => {
    // Check the (first) digit in the word and compare them
    const digit1 = w1.match(/\d/);
    const digit2 = w2.match(/\d/);

    return digit1 < digit2 ? -1:
           digit2 < digit1 ?  1
                           :  0;
  });

  // Connect the words with spaces and remove all digits
  return words.join(" ").replace(/\d/g, "");
}
