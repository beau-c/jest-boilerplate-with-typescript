import { countCharacters } from ".";

it("should return that there is 1 'e' present", () => {
  expect(countCharacters("e", "Hello world!")).toBe(1);
});

it("should return 0 if the character is not present", () => {
  expect(countCharacters("x", "Test!")).toBe(0);
});

it("should return the correct amount of characters that are present", () => {
  expect(countCharacters("b", "Zimbabwe")).toBe(2);
  expect(countCharacters("C", "Chocolate Cookies")).toBe(2);
  expect(countCharacters("2", "111222333")).toBe(3);
  expect(countCharacters("!", "!easy!")).toBe(2);
});

it("should be case sensitive", () => {
  expect(countCharacters("S", "snakes")).toBe(0);
  expect(countCharacters("S", "SnakeS")).toBe(2);
  expect(countCharacters("A", "AAAbbbccc")).toBe(3);
});

it("should return 0 for an empty string", () => {
  expect(countCharacters("?", "")).toBe(0);
});

it("should return 0 for an empty character", () => {
  expect(countCharacters("", "what? ? ?")).toBe(0);
});
