export function countCharacters(character, word) {

  let count = 0;

  for(let i=0; i<word.length; i++) {
    if (word.charAt(i) === character) {
      ++count;
    }
  }

  return count;
}
